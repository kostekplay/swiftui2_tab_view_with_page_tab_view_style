////  SwiftUI2_TabViewWithPageTabViewStyleApp.swift
//  SwiftUI2_TabViewWithPageTabViewStyle
//
//  Created on 17/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_TabViewWithPageTabViewStyleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
